# Test for MoabTourque module

```python

>>> from MoabTorqueSubmission import Queue
>>> from MoabTorqueSubmission import JobCluster
>>> from MoabTorqueSubmission import Configuration

>>> conf = Configuration()
... #doctest: +NORMALIZE_WHITESPACE +ELLIPSIS
ENTRY...

>>> QUEUE_OPTIONS = {
...     "fvst_ssi_dtu": {"A": "fvst_ssi_dtu", "W": "group_list=fvst_ssi_dtu"},
...     "cge": {"A": "cge", "W": "group_list=cge"}
... }

>>> jc = JobCluster(V=True, d="tests", outdir="tests",
...                 errdir="tests", scripts="tests",
...                 job_interval=1, enable_balancing=True,
...                 use_full_job_id=True, **QUEUE_OPTIONS["fvst_ssi_dtu"])
... #doctest: +NORMALIZE_WHITESPACE +ELLIPSIS
ENTRY...

>>> cmd1 = "sleep 30; touch moabtestqueue_tmp_file\n"
>>> jobid1 = "moabqueue_test_01"
>>> jc.addJob(jobid=jobid1, cmd=cmd1, nodes=1,
...                  ppn=1, mem="900m", walltime=600)

>>> cmd2 = "rm moabtestqueue_tmp_file; sleep 60; touch moabtestqueue_tmp_file2\n"
>>> jobid2 = "moabqueue_test_02"
>>> jc.addJob(jobid=jobid2, cmd=cmd2, nodes=1,
...                  ppn=1, mem="900m", walltime=600, depend=[jobid1])

>>> job_id_tuple = tuple(jc.keys())
>>> print(job_id_tuple)
('moabqueue_test_01', 'moabqueue_test_02')

>>> jc.run(job_wait=True, max_wait=600)


```

