#! /tools/bin/python3

import subprocess
import time
import os
import os.path
import argparse
import re
import sys
import math
import inspect

# Adding folder of script to python path
script_file = inspect.getfile(inspect.currentframe())
script_dir = os.path.split(script_file)[0]
script_dir_abs = os.path.realpath(os.path.abspath(script_dir))
if script_dir_abs not in sys.path:
    sys.path.insert(0, script_dir_abs)

# Adding folder of submodule Dependencies to python path
dep_dir = os.path.join(script_dir, "python_module_dependencies")
dep_dir_abs = os.path.realpath(os.path.abspath(dep_dir))
if dep_dir_abs not in sys.path:
    sys.path.insert(0, dep_dir_abs)

from python_module_dependencies.Dependencies import Dependencies

# Usage: Create a jobcluster object.
# add jobs to JobCluster.
#   *jobs can be dependent on other jobs, either by id or by pattern[TODO].
#   *job resources are specified per job and default can be set when creating
#    the cluster.
# [TODO]delete jobs from cluster.
# run jobs in cluster.
# TODO: Queue efficiency. Control (also) with conf file.


# Check configuration file.
# config = (os.path.dirname(os.path.realpath(__file__)) +
#          "/moabtorquesubmission_config.txt")
# if(not os.path.isfile(config)):
#    print("configuration file not found:", config)
#    quit(1)
# global prgs
# prgs = Dependencies(config, executables=False)


class QueueWalltimeError(Exception):
    ''' Raise when max wait time is exceeded and a job hasn't finished.'''
    def __init__(self, message, *args):
        self.message = message
        # allow users initialize misc. arguments as any other builtin Error
        super(QueueWalltimeError, self).__init__(message, *args)


class Configuration(dict):
    def __init__(self, dependencies_file=None):
        if(dependencies_file is None):
            dependencies_file = (os.path.dirname(os.path.realpath(__file__))
                                 + "/moabtorquesubmission_config.txt")
        self.prgs = Dependencies(dependencies_file, executables=False)


class JobCluster(dict):
    ''' JobCluster inherits dict, stores job objects.
        job dict contains jobid(str)->Job(Job). It is not recommended to use
        the job dict directly, instead the methods provided should be used.
        A job will be created with a single key poitining to it in the
        JobCluster dict. But when the job is submitted a new key based on the
        queue ID will also be created, but pointing to the same job object.
    '''

    def __init__(self, a="", A="", d="", E="", h=False, j="", m="",
                 p="", q="", r=False, S="", v="", V=False, W="", nodes="",
                 ppn="", mem="", walltime="", outdir="./", errdir="./",
                 scripts="./", job_interval=0, enable_balancing=False,
                 use_full_job_id=False, max_procs_use=None,
                 dependencies_file=None, conf_file=None,
                 ignore_queue_load=False):
        ''' Constructor - can be empty.
            All options given here will be the default for the jobs in the
            cluster.
            outdir: directory in which output logs are stored.
            errdir: directory in which error logs are stored.
            scripts: directory in which the shell scripts submitted to the
                     queue are stored.
            enable_balancing: Creates limits for the amount of jobs that are
                              allowed to be running simultaniously. This
                              balancing is based on the "queue_conf.txt" file
                              in the script directory.

            self (JobCluster(dict)) - k:jobid, v:Job - starts empty.
            self.options (tuple) - Contains default options for cluster.
            self.outdir (str) - directory to output logs.
            self.errdir (str) - directory to error logs.
            self.scripts (str) - directory to scripts files.
        '''

        # Load dependencies
        if(dependencies_file is None):
            dependencies_file = (os.path.dirname(os.path.realpath(__file__))
                                 + "/moabtorquesubmission_config.txt")
        if(not os.path.isfile(dependencies_file)):
            print(("configuration file for queue module dependencies not "
                   "found:" + dependencies_file))
            quit(1)

        self.conf = Configuration(dependencies_file)

        self.outdir = outdir
        self.errdir = errdir
        self.scripts = scripts
        self.job_interval = job_interval
        self.jobs_w_queue_id = {}
        self.run_started = False
        self.use_full_job_id = use_full_job_id

        if(nodes):
            self.nodes = nodes
        if(ppn):
            self.ppn = ppn
        if(mem):
            self.mem = mem
        if(walltime):
            self.walltime = walltime

        self.options = {}
        if(a):
            self.options["-a"] = a
        if(A):
            self.options["-A"] = A
        if(d):
            self.options["-d"] = d
        if(E):
            self.options["-E"] = E
        if(h):
            self.options["-h"] = ""
        if(j):
            self.options["-j"] = j
        if(m):
            self.options["-m"] = m
        if(p):
            self.options["-p"] = p
        if(q):
            self.options["-q"] = q
        if(r):
            self.options["-r"] = ""
        if(S):
            self.options["-S"] = S
        if(v):
            self.options["-v"] = v
        if(V):
            self.options["-V"] = ""
        if(W):
            self.options["-W"] = W
        tuple(self.options)

        # Retrieve which user owns the jobs to be run.
        # whoami_output = subprocess.run(["whoami"], stdout=subprocess.PIPE,
        #                                shell=True)
        # self.owner = whoami_output.stdout.decode("utf-8").strip()

        self.queue = Queue(procs_limit=max_procs_use,
                           ignore_queue_load=ignore_queue_load,
                           conf=self.conf)
        self.control = None
        if(enable_balancing):
            if(conf_file is None):
                conf_file = (os.path.dirname(os.path.realpath(__file__))
                             + "/queue_conf.txt")
            self.control = JobControl(self.queue, conf_file=conf_file)

    def addJob(self, jobid, cmd, a="", A="", d="", E="", e="", h=False, j="",
               m="", o="", p="", q="", r=False, S="", v="", V=False, W="",
               nodes="", ppn="", mem="", walltime="", depend=""):
        ''' Add job object to JobCluster.
            Required args: jobid, cmd, nodes, ppn, mem, and walltime.
                jobid: have to be unique and will become the name of the job in
                       queue.
                cmd: Text string containing the job commands to be executed.
            The other required args are not required if given in the JobCluster
            object.
            depend should be a list of ids already in the JobCluster object.
            Unless e and/or o arguments are passed, log files will be named
            jobid.err and jobid.out, respectively.
        '''

        #
        #  Check required arguments.
        #
        error = ""
        if(not nodes):
            if(not self.nodes):
                error += "nodes "
            else:
                nodes = self.nodes
        if(not ppn):
            if(not self.ppn):
                error += "ppn "
            else:
                ppn = self.ppn
        if(not mem):
            if(not self.mem):
                error += "mem "
            else:
                mem = self.mem
        if(not walltime):
            if(not self.walltime):
                error += "walltime "
            else:
                walltime = self.walltime

        if(error):
            # TODO Throw some kind of Exceptionnot
            error = "Missing arguments for job " + jobid + ": " + error
            print(error)
            quit()

        if(jobid in self):
            # TODO Throw some kind of exceptionnot
            print(jobid,
                  " already exists in job cluster. Job IDs must be unique.")
            quit(1)

        new_job = _Job(jobid=jobid, cmd=cmd, conf=self.conf, a=a, A=A, d=d,
                       E=E, e=e, h=h, j=j, m=m, o=o, p=p, q=q, r=r, S=S, v=v,
                       V=V, W=W, nodes=nodes, ppn=ppn, mem=mem,
                       walltime=walltime, depend=depend, cluster=self)

        self[jobid] = new_job

    def run(self, job_wait=False, max_wait=0, execute=True):
        ''' Submits all jobs in cluster.
            Initiates the recursive method _run_job.
        '''
        if(self.run_started):
            print("ERROR: This job cluster has already been run once.",
                  file=sys.stderr)
            quit(1)
        else:
            self.run_started = True

        if(not self.keys()):
            print("WARN: Attempted to run an empty job cluster.",
                  file=sys.stderr)
            return

        wait_dict = {}
        submitted_dict = {}
        # Make a copy of the keys in dict as keys are added to the dict while
        # iterating through it.
        job_id_tuple = tuple(self.keys())

        for jobid in job_id_tuple:
            if(jobid not in submitted_dict):
                self._run_job(jobid, wait_dict, submitted_dict,
                              execute=execute)

        if(job_wait):
            self._wait_for_jobs(max_wait)

    def _run_job(self, jobid, wait_dict, submitted_dict, execute=True):
        ''' Recursive method.
            Each job is executed unless it has dependencies,
            then the dependencies are beeing executed.
            A "wait" dict is maintained and passed on to make
            sure no infinete loops are created.
        '''
        # This is only to give the queue some breathing room. Often just 0.
        time.sleep(self.job_interval)

        job = self[jobid]
        if(job.depend):
            dependencies_complete = 1
            wait_dict[jobid] = 1
            depend_queue_ids = []
            for depend_id in job.depend:
                # Dependency has been submited.
                if(depend_id in submitted_dict):
                    queue_id = self[depend_id].queue_id[0]
                    depend_queue_ids.append(queue_id)
                # Infinite loop detected - quit!
                elif(depend_id in wait_dict):
                    # TODO throw exception or whatever python does here!
                    print("Infinite loop of dependencies detected.",
                          jobid, "depends on ", depend_id,
                          "that is itself waiting on a dependency.")
                    quit()
                # Recursive call with dependency id.
                else:
                    dependencies_complete = 0
                    self._run_job(depend_id, wait_dict, submitted_dict,
                                  execute=execute)

            # If calls were made with dependencies, these are not included in
            # dependencies list. Call needs to be made again to create list.
            if(not dependencies_complete):
                self._run_job(jobid, wait_dict, submitted_dict,
                              execute=execute)
                return

            # All dependencies has been submitted. Submit current job.
            job.run(depend_queue_ids=depend_queue_ids, execute=execute)
            submitted_dict[jobid] = 1
            if jobid in wait_dict:
                del wait_dict[jobid]
        else:
            # No dependencies. Submit job.
            submitted_dict[jobid] = 1
            if jobid in wait_dict:
                del wait_dict[jobid]
            job.run(execute=execute)

    def _wait_for_jobs(self, max_wait=0):
        ''' Internal method used by the run method to wait for all jobs to
            finish.
        '''
        all_jobs_not_completed = True
        start_time = time.time()
        while(all_jobs_not_completed):
            current_time = time.time()
            if(max_wait != 0 and max_wait < (current_time - start_time)):
                err_msg = ("ERROR: Job killed. Exceeded max wait time: " +
                           str(max_wait))
                print(err_msg, file=sys.stderr)
                raise(QueueWalltimeError(err_msg))
            time.sleep(180)
            self.update_job_states()

            for job in self:
                queue_id = self[job].queue_id[0]
                if(queue_id):
                    if(self.jobs_w_queue_id[queue_id] != "C"):
                        all_jobs_not_completed = True
                        break
                    else:
                        all_jobs_not_completed = False
                # This should never happen... All jobs should have queue IDs at
                # this point.
                else:
                    print("WARNING: Not all jobs has queue ids!?",
                          file=sys.stderr)
                    all_jobs_not_completed = True
                    break

    def update_job_states(self):
        ''' Update the status of all jobs found in the cluster jobs_w_queue_id
            dict.
        '''
        self.queue.update()
        for queue_id in self.jobs_w_queue_id:
            current_status = self.jobs_w_queue_id[queue_id]
            if(current_status == "C"):
                # (C)omplete is the final state, no need to update any further.
                print(queue_id + " still is complete", file=sys.stderr)
                continue
            else:
                if(queue_id in self.queue):
                    print(queue_id + " is set to " + self.queue[queue_id],
                          file=sys.stderr)
                    self.jobs_w_queue_id[queue_id] = self.queue[queue_id]
                # If the job is not found it is assumed to be complete.
                else:
                    print(queue_id + " is assumed complete", file=sys.stderr)
                    self.jobs_w_queue_id[queue_id] = "C"


class _Job(object):
    ''' Internal class.
        This class is only handled through a JobCluster object.
    '''

    def __init__(self, jobid, cmd, cluster, conf, a="", A="", d="", E="", e="",
                 h=False, j="", m="", o="", p="", q="", r=False, S="", v="",
                 V=False, W="", nodes="", ppn="", mem="", walltime="",
                 depend=""):
        self.jobid = jobid
        self.cmd = cmd
        self.cluster = cluster
        self.depend = depend
        tuple(self.depend)
        self.queue_id = ""  # Empty when job hasn't been submitted.

        if(ppn):
            self.ppn = ppn
        else:
            self.ppn = cluster.ppn

        if(nodes):
            self.nodes = nodes
        else:
            self.nodes = cluster.nodes

        self.req_procs = int(nodes) * int(ppn)

        if(not mem):
            mem = cluster.mem

        if(not walltime):
            mem = cluster.walltime

        #
        # Handle flags
        #
        self.options = {}
        if(a):
            self.options["-a"] = a
        if(A):
            self.options["-A"] = A
        if(d):
            self.options["-d"] = d
        if(E):
            self.options["-E"] = E
        if(e):
            self.options["-e"] = self.cluster.errdir + "/" + e
        else:
            self.options["-e"] = (self.cluster.errdir + "/" + self.jobid
                                  + ".err")
        if(h):
            self.options["-h"] = ""
        if(j):
            self.options["-j"] = j
        if(m):
            self.options["-m"] = m
        if(o):
            self.options["-o"] = self.cluster.outdir + "/" + o
        else:
            self.options["-o"] = (self.cluster.outdir + "/" + self.jobid
                                  + ".out")
        if(p):
            self.options["-p"] = p
        if(q):
            self.options["-q"] = q
        if(r):
            self.options["-r"] = ""
        if(S):
            self.options["-S"] = S
        if(v):
            self.options["-v"] = v
        if(V):
            self.options["-V"] = ""
        if(W):
            self.options["-W"] = W

        # Get all flags that has been set in cluster object.
        for flag in cluster.options:
            # Skip flags that need special handling
            if(flag == "nodes" or flag == "ppn" or flag == "mem"
               or flag == "walltime"):
                continue

            # Job options takes precedence over cluster options.
            if flag in self.options:
                next

            flag_val = cluster.options[flag]

            # Check if the flag has an attached value/string
            if(flag_val):
                self.options[flag] = flag_val
            else:
                self.options[flag] = ""

        #
        # Create job cmd
        #
        self.job_cmd = conf.prgs["qsub"] + " -N " + jobid

        for flag in self.options:
            flag_val = self.options[flag]
            if(flag_val):
                self.job_cmd += " " + flag + " " + flag_val
            else:
                self.job_cmd += " " + flag

        # The resource part must be last so dependencies can be added.
        self.job_cmd += (" -l nodes=" + str(nodes) + ":ppn=" + str(ppn))
        self.job_cmd += ",mem=" + mem + ",walltime=" + str(walltime)

    def update_dependencies(self, depend_queue_ids):
        ''' The method takes a list of queue ids and returns a list of ids that
            are still "active" in the queue (i.e. not completed)
        '''
        # Update queue
        self.cluster.queue.update()

        cleaned_queue_ids = []
        for queue_id in depend_queue_ids:
            # Get queue status of job.
            depend_status = self.cluster.queue.get(queue_id, None)
            # If queue ID is not found it's assumed to be completed.
            # Completed jobs are not added to the dependency list.
            if(depend_status):
                if(depend_status != "C" and depend_status != "E"):
                    cleaned_queue_ids.append(queue_id)

        # The new list without completed dependencies is returned
        return cleaned_queue_ids

    def create_dependecies_flag(self, depend_queue_ids):
        ''' From a list of dependencies ids, creates a dependencies string for
            the qsub command.
            Return the empty string if all depen are completed.
        '''
        depend_str = ""
        if(not depend_queue_ids):
            return depend_str

        # Convert job ids to full job ids if necessary.
        if(self.cluster.use_full_job_id):
            depend_full_queue_ids = []
            for queue_id in depend_queue_ids:
                dep_job = self.cluster.jobs_w_queue_id[queue_id]
                # The job queue id is a tuple with (queue_id, full_queue_id)
                depend_full_queue_ids.append(dep_job.queue_id[1])
            depend_queue_ids = depend_full_queue_ids

        depend_str = depend_queue_ids.pop()
        for queue_id in depend_queue_ids:
            depend_str += ":" + queue_id

        if(depend_str):
            depend_str = "-W depend=afterany:" + depend_str
            return depend_str
        else:
            return ""

    def run(self, depend_queue_ids=0, execute=True):
        # Create script and run!

        if(self.cluster.control):
            self.cluster.control.update()
            self.cluster.queue.update()

            # Get active jobs belonging to the cluster.
            active_jobs = self.cluster.queue.get_jobs_with_status(
                ("R", "E", "H", "Q", "T", "W", "S"), cluster=self.cluster)
            # Get number of processors used by the active jobs.
            active_procs = 0
            for job in active_jobs:
                active_procs += self.cluster.jobs_w_queue_id[job].req_procs

            free_procs = (self.cluster.queue.procs_avail -
                          self.cluster.queue.procs_used)

            sleep_freq = 300

            # While max jobs has been reached and the minimum free space has
            # been passed, wait.
            # This means if there is plenty of free space then submit job. And
            # If there is not enough free space but maximum of running or
            # queued jobs hasn't been reached, then submit jobs.
            while(active_procs >= self.cluster.control.max_jobs and
                  free_procs <= self.cluster.control.min_free_space):

                time.sleep(sleep_freq)

                self.cluster.control.update()
                self.cluster.queue.update()

                # Get active jobs belonging to the cluster.
                active_jobs = self.cluster.queue.get_jobs_with_status(
                    ("R", "E", "H", "Q", "T", "W", "S"), cluster=self.cluster)
                # Get number of processors used by the active jobs.
                active_procs = 0
                for job in active_jobs:
                    active_procs += self.cluster.jobs_w_queue_id[job].req_procs

                free_procs = (self.cluster.queue.procs_avail -
                              self.cluster.queue.procs_used)

            if(depend_queue_ids):
                depend_queue_ids = self.update_dependencies(depend_queue_ids)

        # Assembles the dependencies string for submit job command.
        depend_str = ""
        if(depend_queue_ids):
            depend_str = self.create_dependecies_flag(depend_queue_ids)

        submit_cmd = self.job_cmd

        # Handle dependencies flag.
        if(depend_str):
            submit_cmd += " " + depend_str

        shell_file = self.cluster.scripts + "/" + self.jobid + ".sh"

        with open(shell_file, "w", encoding="utf-8") as shell_fh:
            shell_fh.write(self.cmd)
        subprocess.call(["chmod u+x " + shell_file], shell=True)

        if(execute):
            submit_cmd += " " + shell_file

            # Submitting job.
            # Try-except block catches exceptions thrown when the queue could
            # not be reached.
            submit_count = 0
            max_sub_count = 5
            re_sub_freq = 5
            while(max_sub_count > submit_count):
                submit_count += 1
                try:
                    tm = time.asctime()
                    print(tm + "\tSubmiting:", submit_cmd, file=sys.stderr)

                    queue_id_raw = subprocess.check_output([submit_cmd],
                                                           shell=True)
                    time.sleep(3)
                    break
                except:
                    print("!! Error submiting job.", file=sys.stderr)
                    time.sleep(re_sub_freq)
                    # Update dependencies
                    if(submit_count <= 3 and depend_str):
                        print("!! Updating dependencies and resubmitting",
                              file=sys.stderr)
                        # Cut out dependencies flag.
                        re_depend_str = re.compile(
                            r"^(.+) -W depend=afterany\S+? (.+)$")
                        match_depend_str = re_depend_str.search(submit_cmd)
                        left_cmd = match_depend_str.group(1)
                        right_cmd = match_depend_str.group(2)
                        depend_queue_ids = self.update_dependencies(
                            depend_queue_ids)
                        depend_str = self.create_dependecies_flag(
                            depend_queue_ids)
                        submit_cmd = (left_cmd + " " + depend_str + " "
                                      + right_cmd)
                    elif(submit_count <= 3):
                        re_sub_freq = 120
                        print("!! Resubmitting job.", file=sys.stderr)
                    # Try and remove all dependencies if job keeps failing.
                    if(submit_count > 3):
                        re_depend_str = re.compile(
                            r"^(.+) -W depend=afterany\S+? (.+)$")
                        match_depend_str = re_depend_str.search(submit_cmd)
                        if(match_depend_str):
                            left_cmd = match_depend_str.group(1)
                            right_cmd = match_depend_str.group(2)
                            submit_cmd = left_cmd + " " + right_cmd
                            print("Trying new cmd str: " + submit_cmd,
                                  file=sys.stderr)
                            re_sub_freq = 600
                    elif(max_sub_count == submit_count):
                        print("Giving up submission.",
                              file=sys.stderr)
                        quit(1)

            #
            # Correcting issue JOIDHOST20160718
            #

            # Remember check_output returns a byte str.
            # self.queue_id = self.queue_id.decode('utf-8').strip()
            queue_id_complete = queue_id_raw.decode('utf-8').strip()

            # re_queue_id = re.compile(r"^((\S+)\.cm\.cluster|(\S+))")
            re_queue_id_no = re.compile(r"^(\d+)")
            match_queue_id_no = re_queue_id_no.search(queue_id_complete)
            if(match_queue_id_no):
                self.queue_id = (match_queue_id_no.group(1), queue_id_complete)

            # COMMENTED: after the queue update the entire id (including host)
            # is now needed. This code removed the host part.
            # Issue JOIDHOST20160718.
            # Sometimes queue ids will be returned as <id>.<host>
            # only the <id> can be used.
            # re_queue_id = re.compile(r"^(\d+)")
            # match_queue_id = re_queue_id.search(self.queue_id)
            # if(match_queue_id):
            #    self.queue_id = match_queue_id.group(1)

            # Submitted jobs has two pointers to the _Job object. One is the
            # name and the other is the queue ID number.
            self.cluster[self.queue_id[0]] = self
            self.cluster.jobs_w_queue_id[self.queue_id[0]] = self

            tm = time.asctime()

            print(tm + "\tSubmitted\tid: " + self.queue_id[1],
                  file=sys.stderr)


class Queue(dict):
    '''
    '''
    def __init__(self, conf, procs_limit=None, ignore_queue_load=False):
        self.procs_avail = procs_limit
        self.procs_limit = procs_limit
        self.procs_used = None
        self.prgs = conf.prgs

        if(ignore_queue_load):
            self.procs_used = 0
        else:
            self.update()  # Sets procs_avail and used.

    def update(self):
        '''
        '''
        # Check node states.
        # Find number of processors available in the queue

        # Get the jobs stored from the previous update call, if any.
        existing_jobs = set(self.keys())

        pbsnodes_output = False
        pbsnodes_count = 0
        while(pbsnodes_output is False and pbsnodes_count < 10):
            try:
                pbsnodes_count += 1
                pbsnodes_output = subprocess.check_output(
                    self.prgs["pbsnodes"] + " -a", shell=True)
            except subprocess.CalledProcessError:
                print(("!! Queue failed to respond, attempt "
                      + str(pbsnodes_count) + "\n"
                      "!!\t Attempting again in 5 minutes"), file=sys.stderr)
                pbsnodes_output = False
                time.sleep(300)

        re_node_state = re.compile(r"\s+state = (\S+)")
        re_node_np = re.compile(r"\s+np = (\d+)")

        node_state = None
        node_np = None

        total_available_procs = 0
        up_states = ("job-exclusive", "job-sharing", "reserve", "free", "busy",
                     "time-shared")

        pbsnodes_text = pbsnodes_output.decode("utf-8").split("\n")
        # Loops through pbsnodes output and find pairs of "state" and "np".
        for line in pbsnodes_text:
            match_node_state = re_node_state.search(line)
            # Node state found
            if(match_node_state):
                if(node_state):
                    print("Somethings wrong with node state: " + node_state)
                    quit(1)
                node_state = match_node_state.group(1)
                continue
            match_node_np = re_node_np.search(line)
            # Number of processors for the node is found.
            # This should always follow a "state" match.
            if(match_node_np):
                if(node_np):
                    print("Somethings wrong with node np")
                    quit(1)
                node_np = match_node_np.group(1)
                # If the node is free, the processors are available.
                if(node_state in up_states):
                    total_available_procs += int(node_np)
                    node_np = None
                    node_state = None
                # If the node is not free, the processors are assumed to be
                # offline.
                else:
                    node_np = None
                    node_state = None
                continue

        # Extract all the jobes and their status from the queue
        qstat_cmd = self.prgs["qstat"] + " -a"
        qstat_output = subprocess.check_output([qstat_cmd], shell=True)

        re_jobs = re.compile(r"^(\d+)\S*\s+\S+\s+\S+\s+\S+\s+\S+\s+\d+\s+" +
                             r"(\d+)\s+\S+\s+\S+\s+(\S)\s+\S+$")
        qstat_text = qstat_output.decode("utf-8").split("\n")

        # Parse each line in qstat output.
        currently_registred_jobs = list()
        self.procs_used = 0
        for line in qstat_text:
            line = line.strip()
            match_job = re_jobs.search(line)
            if(match_job):
                currently_registred_jobs.append(match_job.group(1))
                self[match_job.group(1)] = match_job.group(3)
                # Only count processors of jobs (R)unning or (E)nding.
                if(match_job.group(3) == "R" or match_job.group(3) == "E"):
                    self.procs_used += int(match_job.group(2))

        # It can not be assumed a job in the R(unning) state will ever be
        # registred with the C(omplete) state. It may just dissapear from the
        # queue when it completes.
        currently_registred_jobs = set(currently_registred_jobs)
        existing_jobs.difference_update(currently_registred_jobs)
        for job_id in existing_jobs:
            del self[job_id]

        if(self.procs_limit is None):
            self.procs_avail = total_available_procs
        else:
            self.procs_avail = self.procs_limit
        print("Avail procs: " + str(self.procs_avail), file=sys.stderr)
        print("Used procs: " + str(self.procs_used), file=sys.stderr)

    def get_running_and_exiting_jobs(self):
        output_list = []

        for job in self:
            if(self[job] == "R" or self[job] == "E"):
                output_list.append(job)

        return output_list

    def get_jobs_with_status(self, status, cluster=None):
        ''' status must be a tuple of valid states.
            "cluster" is a job_cluster object. If given only jobs with the
            valid state and found in the cluster object are returned.
        '''
        output_list = []

        for job in self:
            if(self[job] in status):
                if(cluster):
                    if(job not in cluster.jobs_w_queue_id):
                        continue
                print("Found job in cluster: " + job + " " + self[job],
                      file=sys.stderr)
                output_list.append(job)

        return output_list

    def get_queued_jobs(self):
        output_list = []

        for job in self:
            if(self[job] == "Q"):
                output_list.append(job)

        return output_list

    def get_completed_jobs(self):
        output_list = []

        for job in self:
            if(self[job] == "C"):
                output_list.append(job)

        return output_list


class JobControl(object):
    '''
    '''
    def __init__(self, queue, conf_file):
        self.efficiency_jobs = [0] * 11
        self.efficiency_space = [0] * 11
        self.efficiency_at_hour = [0] * 24
        self.max_jobs = 0
        self.min_free_space = 1
        self.queue = queue

        self.conf_file = conf_file

        with open(self.conf_file, "r", encoding="utf-8") as conf_fh:
            is_definitions = False
            re_defs = re.compile(r"^(\d+):\s+jobs:(\d+)\s+space:(\S+)")
            for line in conf_fh:
                line = line.strip()
                if(line.startswith("#")):
                    continue
                if(line == "--END OF TIME INTERVALS--"):
                    is_definitions = True
                    continue
                if(is_definitions):
                    line = line.strip()
                    # line is empty
                    if(not line):
                        continue
                    match_defs = re_defs.search(line)
                    if(match_defs):
                        eff_lvl = int(match_defs.group(1))
                        self.efficiency_jobs[eff_lvl] = \
                            int(match_defs.group(2))
                        self.efficiency_space[eff_lvl] = \
                            float(match_defs.group(3))

        self.update()

    def update(self):
        # Get time intervals, time, and set efficiency level accordingly.
        # Getting time intervals every time update is called enables the
        # outside "world" to dynamically change efficiency values.
        index = 0
        prev_eff = 0
        with open(self.conf_file, "r", encoding="utf-8") as conf_fh:
            re_interval = re.compile(r"^(\d+)-(\d+):(\d+)")
            for line in conf_fh:
                line.strip()
                # End of intervals
                if(line == "--END OF TIME INTERVALS--"):
                    break
                # Comments detected
                if(line.startswith("#")):
                    continue
                # Empty line
                if(not line):
                    continue

                match_interval = re_interval.search(line)
                if(match_interval):
                    start = int(match_interval.group(1))
                    end = int(match_interval.group(2))
                    eff = int(match_interval.group(3))

                    # If a timeinterval before the current haven't been defined
                    # It is set to the last known efficiency level or 0.
                    while(index < start and index <= 23):
                        self.efficiency_at_hour[index] = prev_eff
                        index += 1

                    prev_eff = eff

                    while(index >= start and index <= end and index <= 23):
                        self.efficiency_at_hour[index] = eff
                        index += 1

        hour = time.localtime().tm_hour
        active_eff = self.efficiency_at_hour[hour]
        self.max_jobs = self.efficiency_jobs[active_eff]

        exact_min_space = (self.efficiency_space[active_eff] *
                           self.queue.procs_avail)
        print("Exact min space: " + str(exact_min_space), file=sys.stderr)
        self.min_free_space = math.ceil(exact_min_space)

        print("Eff at " + str(hour) + " is set to " + str(active_eff),
              file=sys.stderr)
        print("Max jobs " + str(self.max_jobs) + " Min space: " +
              str(self.min_free_space), file=sys.stderr)


if __name__ == '__main__':

    # Debug
    # job_cluster = JobCluster(enable_balancing=True)
    # print(str(job_cluster.queue.procs_used)+" procs in use of " +
    #      str(job_cluster.queue.procs_avail))
    # r_jobs = job_cluster.queue.get_running_and_exiting_jobs()
    # q_jobs = job_cluster.queue.get_queued_jobs()
    # print("Running jobs: "+str(len(r_jobs)))
    # print("Queued jobs: "+str(len(q_jobs)))
    # quit()

    #
    # Handling arguments
    #
    parser = argparse.ArgumentParser(description="Submit jobs to a MOAB/TORQUE\
                                     queing system.")
    # Posotional arguments
    parser.add_argument("script_dir",
                        help="Directory containing scripts to execute in\
                              queue",
                        metavar='SCRIPT_DIR')

    # Script dir for the objects own scripts
    parser.add_argument("-s", "--scrdir",
                        help="Directory to write internal scripts to.",
                        default=None)
    # Log arguments
    parser.add_argument("-e", "--errdir",
                        help="Directory to write error logs to.",
                        default="error")
    parser.add_argument("-o", "--outdir",
                        help="Directory to write output logs to.",
                        default="output")

    parser.add_argument("--last",
                        help="Path or filename of the script containing the\
                              final job.",
                        default=None)
    parser.add_argument("--wait",
                        help="Number of seconds between checking the status\
                              of the final job. This option must be used along\
                              with --last.",
                        type=int,
                        default=180)
    parser.add_argument("--max_wait",
                        help="Maximum number of seconds to wait before\
                              exiting. Must be used along with --last.",
                        type=int,
                        default=345600)

    parser.add_argument("--job_control",
                        help="Enables job control",
                        type=bool,
                        default=False)

    parser.add_argument("--full_job_id",
                        help="Use full queue job ID. Default is False.",
                        type=bool,
                        default=False)

    # qbot arguments
    parser.add_argument("-a",
                        help="See qsub man page.",
                        default="")
    parser.add_argument("-A",
                        help="See qsub man page.",
                        default="")
    parser.add_argument("-d",
                        help="See qsub man page.",
                        default="")
    parser.add_argument("-E",
                        help="See qsub man page.",
                        default="")
    parser.add_argument("--h_opt",
                        help="See qsub man page.",
                        default=False)
    parser.add_argument("-j",
                        help="See qsub man page.",
                        default="")
    parser.add_argument("-m",
                        help="See qsub man page.",
                        default="")
    parser.add_argument("-p",
                        help="See qsub man page.",
                        default="")
    parser.add_argument("-q",
                        help="See qsub man page.",
                        default="")
    parser.add_argument("-r",
                        help="See qsub man page.",
                        default=False)
    parser.add_argument("-S",
                        help="See qsub man page.",
                        default="")
    parser.add_argument("-v",
                        help="See qsub man page.",
                        default="")
    parser.add_argument("-V",
                        help="See qsub man page.",
                        default="")
    parser.add_argument("-W",
                        help="See qsub man page.",
                        default="")
    parser.add_argument("--nodes",
                        help="See qsub man page.",
                        default="")
    parser.add_argument("--ppn",
                        help="See qsub man page.",
                        default="")
    parser.add_argument("--mem",
                        help="See qsub man page.",
                        default="")
    parser.add_argument("--walltime",
                        help="See qsub man page.",
                        default="")
    parser.add_argument("--job_interval",
                        help="See qsub man page.",
                        type=int,
                        default=1)

    args = parser.parse_args()

    if(args.job_control):
        if(args.job_interval < 10):
            args.job_interval = 10

    # Handle script directory
    if(args.script_dir[-1] == "/"):
        script_filename = args.script_dir[:-1]
    else:
        script_filename = args.script_dir

    if(not args.scrdir):
        args.scrdir = script_filename + "_MoabTorqueSub"
        os.makedirs(args.scrdir, exist_ok=True)
        args.scrdir = os.path.abspath(args.scrdir)

    # Handle log directories
    args.errdir = os.path.abspath(args.errdir)
    os.makedirs(args.errdir, exist_ok=True)
    args.outdir = os.path.abspath(args.outdir)
    os.makedirs(args.outdir, exist_ok=True)

    os.makedirs("out_MoabTorqueSub", exist_ok=True)
    os.makedirs("err_MoabTorqueSub", exist_ok=True)

    # Create Job object
#    jobs = JobCluster(a=args.a, A=args.A, d=args.d, E=args.E, h=args.h_opt,
#                      j=args.j, m=args.m, p=args.p, q=args.q, r=args.r,
#                      S=args.S, v=args.v, V=args.V, W=args.W,
#                      nodes=args.nodes, ppn=args.ppn, mem=args.mem,
#                      walltime=args.walltime, outdir=args.outdir,
#                      errdir=args.errdir, scripts=args.scrdir,
#                      job_interval=args.job_interval)

    jobs = JobCluster(a=args.a, A=args.A, d=args.d, E=args.E, h=args.h_opt,
                      j=args.j, m=args.m, p=args.p, q=args.q, r=args.r,
                      S=args.S, v=args.v, V=args.V, W=args.W, nodes=args.nodes,
                      ppn=args.ppn, mem=args.mem, walltime=args.walltime,
                      outdir="out_MoabTorqueSub", errdir="err_MoabTorqueSub",
                      scripts=args.scrdir, job_interval=args.job_interval,
                      enable_balancing=args.job_control,
                      use_full_job_id=args.full_job_id)

    if(args.last):
        args.last = os.path.basename(args.last)

    last_error_log = ""

    # Retrieve all shell script files
    for file_i in os.listdir(args.script_dir):

        cmd = ""

        if(file_i.endswith(".sh")):
            # Parse script file
            entries_options = ()
            entries_waitfor = ()
            with open(args.script_dir + "/" + file_i,
                      "r", encoding="utf-8") as sh_fh:
                for line in sh_fh:
                    entries = line.strip().split(" ")
                    if(entries[0] == "###OPTIONS"):
                        entries_options = entries
                    elif(entries[0] == "###WAITFOR"):
                        entries_waitfor = entries[1:]
                    elif(entries[0] and entries[0][0] == "#"):
                        continue
                    elif(entries[0]):
                        cmd += line

        # Default options
        N_option = file_i
        V_option = False
        walltime_option = ""
        nodes_option = ""
        ppn_option = ""
        mem_option = ""
        depend_option = ""

        # Parsing ###OPTIONS if found
        if(entries_options):
            for i, entry in enumerate(entries_options):
                if(entry == "-N"):
                    # Not used as the depend part needs this to be the default.
                    # N_option = entries_options[i+1]
                    pass
                if(entry == "-V"):
                    V_option = True
                if(entry == "-l"):
                    l_options = entries_options[i + 1]
                    l_options = l_options.split(",")
                    re_walltime = re.compile(r"walltime=(.+)")
                    re_nodes_ppn = re.compile(r"nodes=(\d+)\:ppn=(\d+)")
                    re_mem = re.compile(r"mem=(.+)")
                    for option in l_options:
                        # Get walltime
                        match_walltime = re_walltime.search(option)
                        if(match_walltime):
                            walltime_option = match_walltime.group(1)
                            continue
                        # Get nodes and ppn
                        match_nodes_ppn = re_nodes_ppn.search(option)
                        if(match_nodes_ppn):
                            nodes_option = match_nodes_ppn.group(1)
                            ppn_option = match_nodes_ppn.group(2)
                            continue
                        # Get memory requirements
                        match_mem = re_mem.search(option)
                        if(match_mem):
                            mem_option = match_mem.group(1)
                            continue

        # Add logging to the script
        cmd_lines = cmd.split("\n")

        error_log = args.errdir + "/" + N_option + ".err"
        output_log = args.outdir + "/" + N_option + ".out"

        cmd = "#!/bin/bash\n"
        cmd += "touch " + error_log + " " + output_log + "\n"

        for line in cmd_lines:
            if(line == ""):
                continue
            cmd += line + " 1>> " + output_log + " 2>> " + error_log + "\n"

        if(args.last == N_option):
            cmd += ("echo \"MoabTorqueSub: final job completed\" &>> "
                    + error_log + "\n")

        # Parsing ###WAITFOR if found
        if(entries_waitfor):
            depend_option = []
            for entry in entries_waitfor:
                entry = entry.strip()
                depend_option.append(entry)

        jobs.addJob(jobid=N_option, cmd=cmd, nodes=nodes_option,
                    ppn=ppn_option, mem=mem_option, walltime=walltime_option,
                    depend=depend_option)

        if(args.last):
            if(N_option == args.last):
                last_error_log = error_log

    if(args.last and not last_error_log):
        print("Error: The final script indicated with the --last flag was not"
              "found: " + args.last)
        quit(1)

    # Submit all jobs
    print("# Submitting", len(jobs), "jobs.")
    if(args.last):
        if(args.max_wait):
            jobs.run(job_wait=True, max_wait=args.max_wait)
        else:
            jobs.run(job_wait=True)
    else:
        jobs.run()

    # Wait for final job to finish if specified
    # if(args.last):
    #    print("Waiting for "+args.last+" to finish...")
    #    last_completed = False
    #    while(not last_completed):
    #        time.sleep(args.wait)
    #        if(os.path.isfile(last_error_log)):
    #            with open(last_error_log, "r", encoding="utf-8") as last_fh:
    #                for lines in last_fh:
    #                    lines = lines.rstrip()
    #                    if(lines == "MoabTorqueSub: final job completed"):
    #                        last_completed = True

    quit(0)
