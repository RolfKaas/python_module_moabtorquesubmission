# Adding module as a submodule #


```
#!bash
git submodule add https://RolfKaas@bitbucket.org/RolfKaas/python_module_moabtorquesubmission.git

# Needed because the submission module itself contains the submodule Dependencies
git submodule update --init --recursive
```