# IMPORTANT
# The tests found in this file is only meant to work on the internal CGE
# servers.
#
# Note: In a full queue, each test takes approximately 5-8 minutes.

import unittest
import sys
import os
import shutil
from subprocess import PIPE, run
from contextlib import redirect_stderr

# This is not best practice but for testing, this is the best I could come up
# with
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
from MoabTorqueSubmission import JobCluster


python3 = "/home/data1/tools/bin/Anaconda3-2.5.0/bin/python"
root_test_dir = os.path.abspath(os.path.dirname(__file__))
run_test_dir = root_test_dir + "/running_test"
dummy_file = "dummy_wait_file.txt"
conf_file = "moabtorquesubmission_config.txt"
queue_conf_file = "queue_conf.txt"
test_names = ["test1", "test2"]


class MoabTorqueSubmissionTest(unittest.TestCase):

    def setUp(self):
        # Change working dir to test dir
        os.chdir(root_test_dir)

        # Does not allow running two tests in parallel
        os.makedirs(run_test_dir, exist_ok=False)
        with open(run_test_dir + "/" + dummy_file, "w") as fh:
            fh.write("File with some test content\n")

    def tearDown(self):
        os.chdir(root_test_dir)
        shutil.rmtree(run_test_dir)

    def test_create_jobs_with_dependencies_as_script(self):
        # Maria has decided she needs to submit jobA first and then run jobB
        # when jobA finishes.
        # For some reason, Maria decided to write her code in shell and is
        # therefore not able to use MoabTorqueSubmission as a module. Luckely
        # she is also able to is MoabTorqueSubmission as a script.

        # Maria creates some directories for her job
        test_dir = run_test_dir + "/" + test_names[0]
        os.makedirs(test_dir)
        test_dir = os.path.abspath(test_dir)
        os.chdir(test_dir)

        script_dir = test_dir + "/scripts"
        os.makedirs(script_dir)

        # Then Maria creates the scripts that are supposed to execute what she
        # need to be done.
        script1 = "jobA.sh"
        cmd1 = ("cd " + test_dir + "\n"
                "sleep 120\n"
                "cp ../" + dummy_file + " .")
        MoabTorqueSubmissionTest.create_job_script(
            cmd=cmd1,
            options=("-N \"CSI_test_job1\" -V -m e "
                     "-l walltime=300,1:ppn=1,mem=900m"),
            out=script_dir + "/" + script1,
            shebang="#!/usr/bin/sh"
        )

        script2 = "jobB.sh"
        MoabTorqueSubmissionTest.create_job_script(
            cmd="echo 'Found a dummy file!'\n",
            options=("-N \"CSI_test_job2\" -V -m e "
                     "-l walltime=300,1:ppn=1,mem=900m"),
            out=script_dir + "/" + script2,
            shebang="#!/usr/bin/sh",
            waitfor=script1,
            wait_file=test_dir + "/" + dummy_file
        )

        # And finally, Maria uses MoabTorqueSubmission to submit her jobs.
        job_procs = run(
            python3 + " ../../../MoabTorqueSubmission.py --job_control True "
            "--full_job_id True -e error -o output --last " + script2 + " "
            + script_dir, shell=True, stderr=PIPE, stdout=PIPE
        )

        # The Maria waits for her jobs to finish.

        # When the jobs are finished, Maria checks the error log to see if the
        # jobs finished as they should.
        errs = job_procs.stderr.decode()
        self.assertNotIn("Error", errs, "Error found in stderr log: " + errs)
        self.assertNotIn("killed", errs, "killed found in stderr log: " + errs)
        self.assertNotIn("Traceback", errs, "Traceback found in stderr log: "
                         + errs)
        self.assertNotIn(".pl", errs, ".pl found in stderr log: " + errs)

    def test_create_jobs_with_dependencies_as_module(self):
        # Maria wants to use MoabTorqueSubmission as a module in her python3
        # script.

        # Maria creates some directories for her job
        test_dir = run_test_dir + "/" + test_names[1]
        os.makedirs(test_dir)
        test_dir = os.path.abspath(test_dir)
        os.chdir(test_dir)

        script_dir = test_dir + "/scripts"
        os.makedirs(script_dir)

        # Maria starts by creating a JobCluster object, that will hold all her
        # jobs, and queue information.
        queue_log = "queue.log"
        with open(queue_log, "a") as stderr, redirect_stderr(stderr):
            jobs = JobCluster(V=True, d=test_dir, outdir=test_dir,
                              errdir=test_dir, scripts=script_dir,
                              job_interval=1, enable_balancing=True,
                              use_full_job_id=True,
                              dependencies_file="../../../" + conf_file,
                              conf_file="../../../" + queue_conf_file)

        # Maria creates the first command that needs to run in the queue.
        cmd1 = ("cd " + test_dir + "\n"
                "sleep 120\n"
                "cp ../" + dummy_file + " .")

        # She gives the job an ID
        job1_name = "cgebase_test_job1"

        # ...and then adds the job to the JobCluster object.
        jobs.addJob(jobid="cgebase_test_job1", cmd=cmd1, nodes=1,
                    ppn=1, mem="900m", walltime=300)

        # Marias 2nd job will depend on her first, so she needs to create a
        # list of job ids. Of cause, in this case this will just be a list of
        # one.
        job_ids = list()
        job_ids.append(job1_name)

        # Maria creates the code for the second job.
        cmd2 = "echo 'Found a dummy file!'\n"

        # ...and then adds the second job to the queue, with the depend
        # argument.
        job2_name = "cgebase_test_job2"
        jobs.addJob(jobid=job2_name, cmd=cmd2, nodes=1,
                    ppn=1, mem="900m", walltime=300, depend=job_ids)

        # Finally, the jobs a submitted.
        with open(queue_log, "a") as stderr, redirect_stderr(stderr):
            jobs.run(job_wait=True)

        # Then Maria waits for her jobs to finish.

        # When the jobs are finished Maria checks the error log.
        with open(queue_log, "r") as fh:
            for i, line in enumerate(fh):
                self.assertNotIn("Error", line,
                                 "Error found in log line: " + str(i))
                self.assertNotIn("killed", line,
                                 "killed found in log line: " + str(i))
                self.assertNotIn("Traceback", line,
                                 "Traceback found in log line: " + str(i))
                self.assertNotIn(".pl", line,
                                 ".pl found in log line: " + str(i))

    @staticmethod
    def create_job_script(cmd, options, out, waitfor=None, wait_file=None,
                          shebang="#!/bin/bash"):
        """
            Creates a script from a command line to be used with qbot

            Arguments (type: hash):
                cmd			Command line to be executed
                out			Write qbot script to this file
                waitfor		Name of scripts to depend on, seperated by
                            whitespaces (only file name, NOT path)
                options		msub options to be passed to the script
                wait_file	Creates a loop in the script that will wait for
                            a specific file to be created with non-zero
                            size. This is to be used with scripts that
                            creates jobs in the queue and then exits, like
                            the "mlst_wrapper" application.
                shebang		Replace shebang. Default: #!/usr/bin/sh
        """
        out_str = (shebang + "\n\n\n"
                   "###OPTIONS " + options + "\n\n")

        if(waitfor):
            out_str += "###WAITFOR " + waitfor + "\n\n"

        out_str += cmd + "\n\n"

        if(wait_file):
            out_str += ("while [ ! -s " + wait_file + " ]\n"
                        "do\n"
                        "    sleep 120\n"
                        "done\n\n")

        out_str += "echo \"qbot: job completed\"\n\n"

        with open(out, "w") as fh:
            fh.write(out_str)

        run("chmod oga+x " + out, shell=True, check=True)


if __name__ == "__main__":
    unittest.main()
